'use strict';
var Model = require('./model');
var inherits = require('util').inherits;

function User(name, password) {
    Model.call(this);

    this.data = Object.freeze({
        name:name,
        password:password
    });

    Model.defineProperties(this, this.data);
}

inherits(User, Model);

module.exports = User;

