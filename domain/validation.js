

module.exports.is8characters = function(password)
{
	if(password == null)
	{
		return false;
	}
	else if(password.length > 7 )
	{
		return true;
	}
	else
	{
		return false;
	}
}
module.exports.containsUppercase = function(password)
{
	for(var i = 0; i <= password.length-1; i++)
		{
			//toUpperCase method can be used to check if a character is uppercase.
			if(password.charAt(i) === password.charAt(i).toUpperCase())
			{				
				return true;
				//In this event as one character is already uppercase we can return true
			}
			else if(password.charAt(i) !== password.toUpperCase()){
				//Found lowercase character.
			}
		}
		//Gone through password list, no uppercase found, return false.
		return false;
}
module.exports.containsLowercase = function(password)
{
	for(var i = 0; i <= password.length-1; i++)
		{
			//toUpperCase method can be used to check if a character is uppercase.
			if(password.charAt(i) === password.charAt(i).toLowerCase())
			{				
				return true;
				//In this event as one character is already uppercase we can return true
			}
			else if(password.charAt(i) !== password.toLowerCase()){
				//Found lowercase character.
			}
		}
		//Gone through password list, no uppercase found, return false.
		return false;
}
module.exports.containsNonAlphaNum = function(password)
{
 for (i = 0, len = password.length; i < len; i++) {
    char = password.charCodeAt(i);
    if (!(char > 47 && char < 58) && // numeric (0-9)
        !(char > 64 && char < 91) && // upper alpha (A-Z)
        !(char > 96 && char < 123)) { // lower alpha (a-z)
      return true;
    }
  }
  return false;
}

//TODO :As this one unit uses multiple units, would testing this method without
//mocking out the other method calls be an integration test? 

module.exports.validPassword = function(password)
{
	if(this.is8characters(password) === true && 
		this.containsNonAlphaNum(password) == true && 
		this.containsUppercase(password) == true && 
		this.containsLowercase(password) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
	

