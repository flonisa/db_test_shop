'use strict';

var Repository = require('./generic_repository');
var User = require('../models/user');
var Model = require('../models/model');
var inherits = require('util').inherits;

function UserRepository(client) {
    
    // console.log("user repostory");
    Repository.call(this, client);
    this.bucketName = 'Person';
}

inherits(UserRepository, Repository);

// UserRepository.prototype.Add = function (object , callback) {
    
//      if (!(object instanceof Model)) {
//         return false
//     }
//     return true;
// };

module.exports = UserRepository;