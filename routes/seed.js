var express = require('express');
var sql 	= require("mssql");
var router 	= express.Router();
var faker 	= require('faker');
var config  = require("../connection/connection.js").config;
var uuid 	= require('node-uuid');

const seed = "ON";
const addresses 	= 50000;
const customers 	= 50000;
const searchTerms	= 100000; //might be a problem inserting so many records.
const products 		= 150000;
const images		= 200000;
const purchases 	= 130000;

/*
 * Incredibly imperfect solution.
 * When generating customers a combination of truncated data 
 * and not dealing with escape characters in the faked string
 * results in many ids being skipped. Might have customerId 1,2,3,5,7 etc. 
 * This leads to problems when generating the id's to those customers, for example in products.
 *
 * Also be careful you don't cancel the request to the page, otherwise it will rerun the reseed method.
 * Trembels in fear with nested for loops. Can't handle them, like at all. 
 */
router.get('/seed', function(req,res,next)
{
	
	sql.connect(config).then(function()
	{
		console.log("connected");
		if(seed === "ON")
		{
			//address
			// generateAddresses(addresses);
			// generateCustomers(customers);
			// generateIps();
			// generateProducts(products);
			// generateSearches(searchTerms);
			// generateImage(images);
			// generateLoginTimes(134000);
			// generateMessages(50000);
			//generatePurchases(purchases);
			// generatePayments(92160);
			// generateTags(100000);
			// generateReviews(50000);
			// generatePurchaseReturns(15000);
			// generateProductTags(10000);
			// generatePurchaseProduct(20000);
			generateUser_Search(50000);
		}


	});
	
	
});

function generateAddresses(loop)
{
	for(i=0; i < loop; i++)
	{
		var street = faker.address.streetName();
		var number = Math.floor((Math.random() * 100) + 1);
		street = street + " " + number;

		var city = faker.address.city();
		var postalCode = faker.address.zipCode();
		var country = faker.address.country();
		//need to test the mysql_real_escape_string.
		var sql = 'insert into address (street, city, postalCode, country) values (\''+mysql_real_escape_string(street)+'\',\''+mysql_real_escape_string(city)+'\', \''+mysql_real_escape_string(postalCode)+'\',\''+mysql_real_escape_string(country)+'\')';

		insertQuery(sql);

	}
}
function generateCustomers(loop)
{
	
	for(i=0; i < loop; i++)
	{
		var fname 		= faker.name.firstName();
		var lname 		= faker.name.lastName();
		var email 		= i+faker.internet.email();
		var phone 		= faker.phone.phoneNumber();

		//dateOfBirth construction
		var year		= Math.floor((Math.random() * 130) + 1890);
		var month		= Math.floor((Math.random() * 11) + 1); 
		var day	 		= Math.floor((Math.random() * 29) + 1); 
		var dateOfBirth = year+"-"+month+"-"+day;
		

		var sex			= Math.floor((Math.random() * 2) + 1);
		var loginName	= faker.internet.userName()+i;
		var password	= faker.internet.password(); 
		//bcrypt would take too long, otherwise we'd do that here.
		var loginAttempts = Math.floor((Math.random() * 5));
		var addressId	= Math.floor((Math.random() * addresses) + 1); 
		var role		= 2; //everyone generated will be a normal user.


		var statement = 'insert into customer (fname, lname, email, phone, dateOfBirth,sex,loginUserName,password,loginAttempts, addressId, role) values (\''+fname+'\',\''+lname+'\', \''+email+'\',\''+phone+'\', \''+dateOfBirth+'\',\''+sex+'\',\''+loginName+'\',\''+password+'\',\''+loginAttempts+'\',\''+addressId+'\',\''+role+'\')';
		insertQuery(statement);


	}
}
function generateIps()
{
	for(i=0; i < 134000 ; i++)
	{
		var ip 				= faker.internet.ip();
		var customerId		= i+1;
		// console.log(ip);
		var statement = 'insert into ips (ipaddress, CustomerId) values (\''+ip+'\',\''+customerId+'\')';
		// console.log(statement);
		insertQuery(statement);
	}
	// console.log("finished inserting Ips");
}
function generateSearches(loop)
{
	for (var i = 0; i < loop; i++)
	{
		
		//aren't enough unique values, need to generate a lot.
		//TODO find more variations. Hopefully 
		console.log(i);
		var rando = Math.floor((Math.random() * 8));
		var searchTerm;
		if(rando === 0)
		{
			searchTerm = faker.commerce.color() + " " + faker.commerce.productAdjective() + " " + faker.commerce.product() + "V." + i;
			// console.log(searchTerm + " " + rando);
		}
		else if(rando === 1)
		{
			searchTerm = faker.address.country() + " " + faker.commerce.productAdjective() + " " + faker.commerce.product() + "Nr."+i;
			// console.log(searchTerm + " " + rando);
		}
		else if(rando === 2)
		{
			searchTerm = faker.company.bsAdjective() + " " + faker.company.bsNoun() + " " +faker.commerce.product() + "#" + i;
			// console.log(searchTerm + " " + rando);
		}
		else if(rando === 3)
		{
			searchTerm = faker.commerce.department() + " " + faker.commerce.productName() + " Prototype nr: " + i;
			// console.log(searchTerm + " " + rando);
		}
		else if(rando === 4)
		{
			searchTerm = faker.commerce.productAdjective() + " " + faker.commerce.color() + " " + faker.commerce.product()+"Ver." + i;
			// console.log(searchTerm+ " " + rando);
		}
		else if(rando === 5)
		{
			searchTerm = faker.commerce.product() + " " + faker.name.firstName() + " " + faker.commerce.productMaterial()+ "So. "+i;
			// console.log(searchTerm+ " " + rando);
		}
		else if(rando === 6)
		{
			searchTerm = faker.commerce.productName() + " " + faker.commerce.color() + " " + faker.commerce.productAdjective() + i;
			// console.log(searchTerm+ " " + rando);
		}
		else if(rando === 7)
		{
			searchTerm = faker.commerce.product() + " Mk."+ i;
			// console.log(searchTerm+ " " + rando);
		}
		var statement = 'insert into search (searchTerm) values (\''+searchTerm+'\')';
		// console.log(i);
		insertQuery(statement);
		
	}
}
function generateProducts(loop)
{
	//customers*2
	for(var i =0; i< loop ; i++)
	{
		var rando = Math.floor((Math.random() * 500) + 1);
		var subcat =  Math.floor((Math.random() * 26) + 1);
		var customerRand = i+1;

		var productName 	= faker.commerce.productName();
		var description 	= faker.lorem.sentences();
		var prodQnty		= Math.floor((Math.random() * 25) + 1);
		var price			= faker.commerce.price();
		var placementDate 	= '2016-08-10 20:49:44';
		var expirationDate 	= '2017-07-10 20:49:44';
		var condition		= "Super-duper!";
		//in the future perhaps loop through an array of the subcategories
		var subcategory		= "Under 1"; 

		var statement = 'insert into product (productName, description,prodQnty,price,placementDate,expirationDate,condition,subcategoryName,customerId) values (\''+productName+'\',\''+description+'\',\''+prodQnty+'\',\''+price+'\',\''+placementDate+'\',\''+expirationDate+'\',\''+condition+'\',\''+subcategory+'\',\''+customerRand+'\')';
		// console.log(statement);
		insertQuery(statement);
	}
}
function generateImage(loop)
{
	var baseUrl = 'http://flonisa/images/'
	for(var i=0; i < loop; i++)
	{
		var imageLocation = baseUrl+uuid.v4();;
		// var productId = i+1;
		var customerId = i+1;
		//the imagename is sometimes too long for the field.
		var imageName = faker.random.word();

		var statement ='insert into image (imageLocation, CustomerId, imageName) values (\''+imageLocation+'\',\''+customerId+'\',\''+imageName+'\')';
		insertQuery(statement)
	}
}
function generateLoginTimes(loop)
{
	for(var i=0; i< loop; i++)
	{

		var year		= Math.floor((Math.random() * 5) + 2010);
		var month		= Math.floor((Math.random() * 11) + 1); 
		var day	 		= Math.floor((Math.random() * 28) + 1); 

		var loginTime = year+"-"+month+"-"+day+' 20:49:44';
		day+=1;
		//conversion fails sometimes here. I guess that's february for you?
		var logoutTime = year+"-"+month+"-"+day +' 13:49:44';
		var customerId = i+1;
		var ipId = i+1;

		// console.log(loginTime);
		// console.log(logoutTime);
		// console.log(customerId);
		// console.log(ipId);

		var statement ='insert into loginTimes (loginTime,logout,customerId,ipId) values (\''+loginTime+'\',\''+logoutTime+'\',\''+customerId+'\',\''+ipId+'\')';
		insertQuery(statement);
	}
}
function generateMessages(loop)
{
	for(var i=0; i< loop; i++)
	{
		var message = faker.lorem.paragraph();
		var messenger = Math.floor((Math.random() * 134000) + 1);
		var messageRecipient = Math.floor((Math.random() * 134000) + 1);

		
		var statement = 'insert into message (message,messengerId,messageRecipientId) values (\''+message+'\',\''+messenger+'\',\''+messageRecipient+'\')';
		// console.log(statement);
		insertQuery(statement);
	}
}
function generatePurchases(loop)
{
	for(var i=0; i< loop; i++)
	{
		//ideally this will be the cumulative of the products in the order. **** ideals im tired.
		var totalAmount = faker.commerce.price();
		var payments = ["pending","paid", "cancelled"]

		var paymentStatus = payments[Math.floor((Math.random() * 3))];

		var year		= Math.floor((Math.random() * 5) + 2010);
		var month		= Math.floor((Math.random() * 11) + 1); 
		var day	 		= Math.floor((Math.random() * 28) + 1); 
		var orderDate 	= year+"-"+month+"-"+day;
		var message 	= faker.lorem.sentence();
		var customerId 	= i+1;

		// console.log(totalAmount);
		// console.log(orderDate);
		// console.log(customerId);

		var statement = 'insert into purchase (totalAmount,paymentStatus,orderDate, message,customerId) values (\''+totalAmount+'\',\''+paymentStatus+'\',\''+orderDate+'\',\''+message+'\',\''+customerId+'\')';
		// console.log(statement);
		insertQuery(statement);
	}
}
function generatePayments(loop)
{
	for(var i=0; i< loop; i++)
	{
		var purchaseId = i+1;
		var paymentDate = '2016-12-11 20:49:44';
		//faking an alphanumeric transactionId
		var transactionId = faker.internet.password() + i;

		var statement = 'insert into payment (purchaseId,paymentDate,transactionId) values (\''+purchaseId+'\',\''+paymentDate+'\',\''+transactionId+'\')';
		insertQuery(statement);

	}
}
function generateTags(loop)
{
	for(var i=0; i< loop; i++)
	{
		var tagName = faker.company.bs() + " |" + i; //just so it's unique.
		var statement = 'insert into tag (tagName) values (\''+tagName+'\')';
		insertQuery(statement);
	}
}
function generateReviews(loop)
{
	for(var i=0; i< loop; i++)
	{

		var rating = Math.floor((Math.random() * 5)+1);
		var review = faker.lorem.paragraph();

		var year		= Math.floor((Math.random() * 130) + 1890);
		var month		= Math.floor((Math.random() * 11) + 1); 
		var day	 		= Math.floor((Math.random() * 29) + 1); 
		
		var reviewTime = year+"-"+month+"-"+day +' 20:49:44';
		var reviewerId = i+1;
		var reviewedId = i+2;

		var statement = 'insert into review (rating, review,reviewTime,reviewedId,reviewerId) values (\''+rating+'\',\''+review+'\',\''+reviewTime+'\',\''+reviewerId+'\',\''+reviewedId+'\')';
		// console.log(statement);
		insertQuery(statement);
		// console.log(review);
	}
}
function generatePurchaseReturns(loop)
{
	for(var i=0; i< loop; i++)
	{
		var problem = faker.lorem.sentence();
		// var createdDate;
		purchaseId = i+1;

		var statement = 'insert into purchaseReturn (problem,purchaseId) values (\''+problem+'\',\''+purchaseId+'\')';
		insertQuery(statement);
	}
}
function generateProductTags(loop)
{
	for(var i = 0; i< loop; i++)
	{
		var productId = i+30000;
		var tagName = "frictionless brand content |3193"

		var statement = 'insert into product_tag (productId,tagName) values (\''+productId+'\',\''+tagName+'\')';
		insertQuery(statement);
	}
}
function generatePurchaseProduct(loop)
{
	for(var i=0; i< loop; i++)
	{
		
		var purchaseId = Math.floor((Math.random() * 276000) + 1); 
		var productId = Math.floor((Math.random() * 270000) + 1);
		var quantity = Math.floor((Math.random() * 45) + 1); 

		var statement = 'insert into purchase_product (purchaseId,productId,quantity) values (\''+purchaseId+'\',\''+productId+'\',\''+quantity+'\')';
		insertQuery(statement);

		
	}
}
function generateUser_Search(loop)
{
	for(var i=0; i< loop; i++)
	{
		var customerId = Math.floor((Math.random() * 130000) + 1);
		var searchId   = Math.floor((Math.random() * 406000) + 1);

		var statement = 'insert into user_search (customerId,searchId) values (\''+customerId+'\',\''+searchId+'\')';
		insertQuery(statement);
	}

}
function insertQuery(queryStatement)
{
			
			//has issues with 's I suspect. using prepared statements would solve that.
			//Now if only we could get that stupid thing to work.
			new sql.Request()
			.query(queryStatement).then(function(recordset) {
				console.log("success");
		        // console.dir(recordset);
		    }).catch(function(err) {
		    	// console.log("query error");
		    	console.log(err); 
		    });
}

//http://stackoverflow.com/a/7760578
function mysql_real_escape_string (str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

module.exports = router;