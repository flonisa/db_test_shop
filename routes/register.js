var express = require('express');
var router 	= express.Router();

//Perhaps move to controller
var config  	= require("../connection/connection.js").config;
var validator 	= require('../domain/validation.js');
var sql 		= require("mssql");
// var bcrypt 		= require("bcrypt-nodejs"); //https://www.npmjs.com/package/bcrypt-nodejs



router.get('/', function(req,res,next)
{
	//TODO add styling.
	res.status(200);
	res.render('home.ejs');

	// var connection = new sql.Connection(config);
	// connection.connect(function(err)
	// {
	// 	console.log(err);
	// });
});
router.post('/', function(req,res,next)
{
	
	//TODO I'd imagine we want to send all the data to the controller.
	//TODO if time better validator class.
	var userName = req.body.user_name
	var firstName = req.body.first_name; 	
	var lastName = req.body.last_name; 		
	var street = req.body.street;
	var postalcode = req.body.postal_code;	
	var city = req.body.city; 				
	var country = req.body.country; 		
	var password = req.body.password;
	var email	=req.body.email;
	//validate
	var validPassword = validator.validPassword(password);
	console.log(validPassword);
	//crude test to see if the required fields exist and the password is valid. 
	if(validPassword == true && userName && firstName && lastName && street && city && country && password && postalcode)
	{
		//hash password
		// var hash = bcrypt.hashSync(password);
		console.log('about to insert');
		sql.connect(config).then(function()
		{
			
			var addressId;
			//2 queries. One to submit to address, the other to customer.
			new sql.Request()
			.query('insert into address (street, city, postalCode, country) values (\''+street+'\',\''+city+'\',\''+postalcode+'\',\''+country+'\') select SCOPE_IDENTITY() as addressId;').then(function(recordset)
			{
				// console.log('success in insert');
				addressId = recordset[0].addressId;

				new sql.Request()
				.query('insert into customer (fname, lname, email, loginUserName,password,loginAttempts, addressId, role) values (\''+firstName+'\',\''+lastName+'\', \''+email+'\',\''+userName+'\',\''+password+'\',\''+'0'+'\',\''+addressId+'\',\''+'2'+'\')').then(function(recordset)
				{
					console.log('succesful registration page');
				}).catch(function(err)
				{
					console.log(err);
				});
				
			}).catch(function(err)
			{
				console.log(err);
			});
		});
}
else{
	res.render('home.ejs', { error: 'validation failed' });
}


console.log('posted register form');
});

module.exports = router;