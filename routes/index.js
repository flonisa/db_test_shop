var express = require('express');
var sql 	= require("mssql");
var router 	= express.Router();
var config  = require("../connection/connection.js").config;
var User  = require("../models/user");
var purchase_repository = require("../repositories/purchase_repository");
var Product  = require("../models/product");
var user_repository = require("../repositories/user_repository");
var product_repository = require("../repositories/product_repository");
var generic_repository = require("../repositories/generic_repository");
// var register = require('register');
/* GET home page. */
router.get('/', function(req, res, next) {

	// GET 4 products. We want name, description, price and creator.
	var products;
	console.log("homepage");
	console.log(req.cookies.test);
	sql.connect(config).then(function()
		{
			console.log('connected');
			new sql.Request()
			.query('SELECT top 4 product.productName, product.description, product.price, Customer.loginUserName FROM product inner join Customer On product.CustomerId = Customer.customerId').then(function(recordset)
			{
				// console.log(recordset);
				
				products = recordset;
				// products = JSON.stringify(products)
				// products = products.replace(/\"([^(\")"]+)\":/g,"$1:");
				// addressId = recordset[0].addressId;
				res.render('index', { products: products });

			}).catch(function(err)
			{
				//error something about changing headers after they are set.
				console.log(err);
			});


		});

	
 	 //console.log(userrepo.selectAll(user));
  	
});

router.get('/test', function(req,res,next)
{
	console.log("alright on the test page");
	

});
router.get('/purchase', function(req,res,next)
{
	console.log("alright on the test page");
	var u = new  purchase_repository('Purchase');
	console.log(u.getBucketName());
	console.log(u.GetAllPurchase(u.getBucketName(),function(count){
		console.log(count);
	}));
});
router.post('/purchase', function(req,res,next)
{
	console.log("alright on the test page");
	var u = new  purchase_repository('Purchase');
	console.log(u.GetByID(16,function(count){
		console.log(count);
	}));
});
router.get('/CreateProduct', function(req,res,next)
{
	res.render('createproduct', { title: 'createproduct' });

});

router.post('/CreateProduct', function(req,res,next)
{

	productrepo = new product_repository('Product');
	var product = new  Product(req.body.productname,req.body.productdec,req.body.productqantity,req.body.price,new Date().toISOString(),new Date().toISOString(),req.body.condiation,'4',req.body.category);
	productrepo.CreateProduct(product)
	res.status(200).send('succeeded!');

	
});

module.exports = router;
