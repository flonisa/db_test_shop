var chai = require('chai');
var assert = chai.assert;
var validation = require('../domain/validation.js');


describe('Field length validation', function(){
	//the individual tests. Start with it. 1st param describes the unit test.
	//2nd param is a function in which we write the test.

	//this very simple test will test if an array starts empty.
	it('Field is 8 characters', function(){

		field = "password";
		assert.equal(validation.is8characters(field), true);
	});
	it('field is 7 characters', function(){
		field = "passwor";
		assert.equal(validation.is8characters(field), false);
	});
	it('field is null', function(){
		field = null;
		assert.equal(validation.is8characters(field), false);
	});
	it('field is empty', function(){
		field = "";
		assert.equal(validation.is8characters(field), false);
	});

});
describe('Field contains an uppercase value', function()
{
	it('field contains one uppercase', function(){
		field = "Password";
		assert.equal(validation.containsUppercase(field), true);
	});
	it('field does not contain an uppercase value', function(){
		field = "password";
		assert.equal(validation.containsUppercase(field), false);
	});
	//check the numbers
	it('field contains a number and no uppercase values', function(){
		field="passwor4";
		assert.equal(validation.containsUppercase(field), false);
	});
	it('field contains a number and an uppercase value', function(){
		field="4Passwor";
		assert.equal(validation.containsUppercase(field), true);
	});

});
describe('Field contains lowercase', function()
{
	it('field contains one lowercase', function(){
		field = "Password";
		assert.equal(validation.containsLowercase(field), true);
	});
	it('field does not contain an Lowercase value', function(){
		field = "PASSWORD";
		assert.equal(validation.containsLowercase(field), false);
	});
	//something funky going on with numbers
	it('field contains a number and no Lowercase values', function(){
		field="PASSWOR4";
		assert.equal(validation.containsLowercase(field), false);
	});
	it('field contains a number and an Lowercase value', function(){
		field="4Passwor";
		assert.equal(validation.containsLowercase(field), true);
	});
});

describe('Field contains one non alphanumeric character', function()
{
	it('field contains one non alphanumeric character', function()
	{
		field="password@";
		assert.equal(validation.containsNonAlphaNum(field), true);
	});
	it('field does not contain one non alphanumeric character', function()
	{
		field="password";
		assert.equal(validation.containsNonAlphaNum(field), false);
	});
	it('field is empty', function()
	{
		field="";
		assert.equal(validation.containsNonAlphaNum(field), false);
	});

});

//Integration test
//I suppose you never know what will happen. Also only 2 possible paths, so might as well.
describe('is a valid password', function(){
	it('is a valid password', function()
	{
		password = "FourChar&";
		assert.equal(validation.validPassword(password), true);
	});
	it('is an invalid password', function()
	{
		password = "no!";
		assert.equal(validation.validPassword(password), false);
	});
});



