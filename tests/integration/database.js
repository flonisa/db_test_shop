const chai = require('chai');
var config  	= require("../connection/connection.js").config;

describe('database inserts', function(){
	var addressId;
	before(function() {

		//TODO recreate and reseed database first.
		//currently Inserting an address and customer.
		//Ideally we'd just have a database schema that would be created that contained these test values.
		//thats' for later.
    	sql.connect(config).then(function()
		{
			
			//2 queries. One to submit to address, the other to customer.
			new sql.Request()
			.query('insert into address (street, city, postalCode, country) values (\'streetTest\',\'cityTest\',\'postalCodeTest\',\'countryTest\') select SCOPE_IDENTITY() as addressId;').then(function(recordset)
			{
				// console.log('success in insert');
				addressId = recordset[0].addressId;

				new sql.Request()
				.query('insert into customer (fname, lname, email, loginUserName,password,loginAttempts, addressId, role) values (\'TestFirstName\',\'TestLastName\', \'TestEmail\',\'TestUserName\',\'TestPassword\',\''+'0'+'\',\''+addressId+'\',\''+'2'+'\')').then(function(recordset)
				{
					
				}).catch(function(err)
				{
					console.log(err);
				});
				
			}).catch(function(err)
			{
				console.log(err);
			});
		});
  	});

	it('customer record is submitted to the database.', function()
	{
		sql.connect(config).then(function()
		{
			new sql.Request()
			.query('SELECT addressId from address where addressId =\''+addressId+'\'').then(function(recordset)
			{
				
			}).catch(function(err)
			{

			})
		});	
	});
	it('address is entered to the database with the above customerId.', function()
	{

	});
});