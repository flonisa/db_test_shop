const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

//https://github.com/chaijs/chai-http
//is this app.js?
const server = require('../../app.js');


describe('routes : registration',function()
{
	
	//before each test we want to rebuild the database with the latest version.
	
	// beforeEach((done) => {

	// })

	describe('GET /register', function(){

		it('should respond with the login page', function(done)
		{
			chai.request(server)
			.get('/register')
			.end(function(err,res)
			{
				should.not.exist(err);
				res.status.should.equal(200);
				done();
			})				
		});
		it('should not respond with an error', function(done)
		{
			chai.request(server)
			.get('/registerss')
			.end(function(err,res)
			{
				// should.not.exist(err);
				res.status.should.equal(404);
				done();
			});
			
		});

	});

	//after each test we want to rollback the database? That seems unnecessary. 
});