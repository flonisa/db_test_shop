var chai = require('chai');
var assert = chai.assert;
var registration = require("../controllers/registration.js");
var purchase_repository = require("../repositories/purchase_repository");
var User = require("../models/user");
var Order_Product  = require('../models/OrderProduct');
var sinon = require('sinon');



describe('Purchase ', function(){

	it('ID P001: Purchase: check if price is in decimal', function(){
		var price = 1.0;
		assert.equal(checkDecimal(price), true);
	});
});

var checkDecimal = function(price){
 return price % 1 === 0;
}



describe('Purchase', function(){

	it('ID P002: Purchase: check total is not less then 0', function(){
		var total = 22.00;
		assert.equal(CheckTotal(total), true);
	});
});

var CheckTotal = function(total){
 if(total <= 0)
 {
 	return false;
 }
 else{ return true ; }
}


describe('Purchase ', function(){

	it('ID P003: purchase quantity not grater than one', function(){
		var orderProduct = new Order_Product('1','1','1');
		assert.equal(checkQuantity(orderProduct.prodQnty), true);
	});
});

var checkQuantity = function(purchase)
{
if(purchase > 1)
{
	return false;
}
else {return true;}
}

describe('Purchase ', function(){

	it('ID P004: Check if the date is in correct formate', function(){
		var date = '03/03/2010';
		assert.equal(checkDate(date), date);
	});
});

var checkDate = function(dateString)
{
  // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}



describe('Purchase', function(){

	it('ID P005: Check GetAllPurchase is called or not', function(){
        var u =new  purchase_repository('Purchase');
           var save = sinon.spy(u, 'GetAllPurchase');
            u.GetAllPurchase(u.getBucketName(),function(c){})
                save.restore();
                sinon.assert.calledOnce(save);
	});
});

describe('Purchase', function(){

    it('ID P005: Check GetAllPurchase is called with parameter', function(){
         var u =new  purchase_repository('Purchase');
             var save = sinon.spy(u, 'GetAllPurchase');
                u.GetAllPurchase(u.getBucketName(),function(c){})
                save.restore();
                sinon.assert.calledWith(save, u.getBucketName());
    });
});

describe('Purchase', function(){
    it('ID P005: Purchase GetByID', function(){
            var u =new  purchase_repository('Purchase');
            var id = '16';
            var database = sinon.mock(u);
            database.expects('GetByID').once().withArgs(id);
            u.GetByID(id);
            database.verify();
            database.restore();
    });
});